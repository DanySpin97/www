DOCS?=$(shell pwd)/docs
IMAGE?=$(shell pwd)/image

all:	$(addsuffix .html,$(basename $(shell find -type f -name '*.mkd' -not -wholename './docs*')))
	@echo $?

%.html:	%.mkd head.part.tmpl foot.part.tmpl contributors.part.tmpl
	@echo "Generating $@ ..."
	@ruby -e "File.open('$<.tmp', 'w') {|file| file.write File.read('$<').gsub(/#\{include ([0-9A-Za-z_.]+)\}/) {|z| File.read(\"#{\$$1}.part.tmpl\")}}"
	@sed -i -e '1 s,^Title: ,Title: Exherbo - ,' $<.tmp
	@sed -i -e 's/^Description: (.*)$/<meta name="description" content="\0" />/' $<.tmp
	@sed -i -r -e 's|`\[pkg:(.*)\]|`\[escapedpkg:\1\]|g' $<.tmp
	@sed -i -r -e 's|`\[::(.*)\]|`\[:repo:\1\]|g' $<.tmp
	@sed -i -r -e 's|\[pkg:repository/(.+)\]|\[`repository/\1`\](//git.exherbo.org/summer/repositories/\1/index.html)|g' $<.tmp
	@sed -i -r -e 's|\[pkg:(.+/.+)\]|\[`\1`\](//git.exherbo.org/summer/packages/\1/index.html)|g' $<.tmp
	@sed -i -r -e 's,\[::(\w+)\],\[`::\1`\](//git.exherbo.org/\1.git),g' $<.tmp
	@sed -i -r -e 's|`\[:repo:(.+)\]|`\[::\1\]|g' $<.tmp
	@sed -i -r -e 's|`\[escapedpkg:(.+)\]|`\[pkg:\1\]|g' $<.tmp
	@maruku --html --output $@ $<.tmp
	@sed -i -e 's,<head>,&<link rel="shortcut icon" href="/favicon.png" />,' $@
	@sed -i -e 's,<td />,<td></td>,g' $@
	@sed -i -e 's,</nav>,</nav><div class="container">,;s,</body>,</div></body>,' $@
	@sed -i -e 's,<html.*xml:,<html ,' $@
	@sed -i -e '/^<?xml/ { N;N;N;d; };s/<html /<!DOCTYPE html>\n<html /' $@
	@sed -i -e 's,<table>,<table class="table">,g' $@
	@sed -i -e 's,</head>,<meta name="viewport" content="width=device-width\, initial-scale=1\, maximum-scale=1\, user-scalable=no" /><link rel="shortcut icon" href="/favicon.ico" /></head>,' $@
	@if [[ "$<" == developers.mkd ]];then $(MAKE) --no-print-directory developers;fi
	@rm $<.tmp

developers: developers.html
	@sed 's|<tbody><tr>|<tbody>\n<tr>|' -i developers.html
	@for dev in $$(grep '^<tr>' developers.html | cut -d'>' -f3 | cut -d'<' -f1);do \
		printf "$$dev... "; \
		gravatar=$$(echo "$$dev" | ./cgit/filters/email-gravatar.sh "$$dev@exherbo.org" 32); \
		sed "s|>$$dev<|>$$gravatar<|;s|>$$dev<d|\&amp;d|" -i developers.html; done
	@echo

deploy:	all
	@if [[ -d "$(IMAGE)" ]];then mkdir -p "$(IMAGE)";fi
	@rsync -a . "$(IMAGE)" \
		--delete-after \
		--exclude "$(IMAGE)" \
		--exclude image \
		--exclude '*.git*' \
		--exclude '*.mkd' \
		--exclude 'Makefile' \
		--exclude '*.tmp' \
		--exclude docs/
	@if [[ -d "$(DOCS)" ]];then $(MAKE) --no-print-directory -C "$(DOCS)" IMAGE="$(IMAGE)/docs" deploy;fi
	@echo "Run \`python2 -m SimpleHTTPServer 8080\` in \"$(IMAGE)\" to view the webpages."

ifdef NO_CONTRIBUTORS

contributors.part.tmpl:
	@echo "Generating stub $@"
	@git config --global user.name >$@

else

contributors.part.tmpl:
	@echo "Generating $@ ..."
	@echo > $@.tmp
	@for repo in ../*/; do ( cd "$${repo}" && git log --format="%aN," ) >> $@.tmp; done
	@sort -fu $@.tmp | tr '\n' ' ' | sed 's/\(.*\)\,/\1\n/' > $@
	@rm $@.tmp

endif

clean:
	rm -rf *.tmp *.html contributors.part.tmpl

.phony:	clean
