#!/bin/sh
# This may be used with the about-filter or repo.about-filter setting in cgitrc.
# It passes formatting of about pages to differing programs, depending on the usage.

# Markdown support requires maruku.
# RestructuredText support requires python and docutils.
# Man page support requires groff.

# The following environment variables can be used to retrieve the configuration
# of the repository for which this script is called:
# CGIT_REPO_URL        ( = repo.url       setting )
# CGIT_REPO_NAME       ( = repo.name      setting )
# CGIT_REPO_PATH       ( = repo.path      setting )
# CGIT_REPO_OWNER      ( = repo.owner     setting )
# CGIT_REPO_DEFBRANCH  ( = repo.defbranch setting )
# CGIT_REPO_SECTION    ( = section        setting )
# CGIT_REPO_CLONE_URL  ( = repo.clone-url setting )

# conversion functions
text() {
    echo "<pre>"
    # escape common html symbols
    sed -e "s/&/\&amp;/g" -e "s/</\&lt;/g" -e "s/>/\&gt;/g" \
        -e "s/\"/\&quot;/g" -e "s/'/\&#39;/g"
    echo "</pre>"
}

markdown() {
    maruku --html-frag
}

# we don't use any of the converters in there
#cd "$(dirname $0)/html-converters/"

case "$(printf '%s' "$1" | tr '[:upper:]' '[:lower:]')" in
    *.md|*.mkd)
        case "${CGIT_REPO_NAME}" in
            docs.git|www.git)
                # don't format markdown for the website repos, because that
                # entails a lot of weird recursion with the headers and such
                # ...it also sounds like it might be unsafe?
                text
            ;;
            *)
                markdown
            ;;
        esac
    ;;
    *.rst)
        exec ./rst2html
    ;;
    *.[1-9])
        exec ./man2html
    ;;
    *.htm|*.html)
        exec cat
    ;;
    *.txt|*)
        text
    ;;
esac
